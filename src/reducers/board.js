import produce from 'immer';
import { LOAD_STATUS } from '../enums';

const initState = {
  status: {
    getAllBoardsStatus: null,
    getBoardStatus: null,
    addBoardStatus: null,
    removeBoardStatus: null,
    updatedBoardTitleStatus: null,
    getNoBoardStatus: null,
  },
  boards: [],
  board: null,
  error: null,
  message: null,
  object: { boardId: null },
};

export const BOARD_TYPES = {
  GET_ALL_BOARDS_REQUEST: 'GET_ALL_BOARDS_REQUEST',
  GET_ALL_BOARDS_FULFILLED: 'GET_ALL_BOARDS_FULFILLED',
  GET_ALL_BOARDS_REJECTED: 'GET_ALL_BOARDS_REJECTED',

  GET_BOARD_REQUEST: 'GET_BOARD_REQUEST',
  GET_BOARD_FULFILLED: 'GET_BOARD_FULFILLED',
  GET_BOARD_REJECTED: 'GET_BOARD_REJECTED',

  ADD_BOARD_REQUEST: 'ADD_BOARD_REQUEST',
  ADD_BOARD_FULFILLED: 'ADD_BOARD_FULFILLED',
  ADD_BOARD_REJECTED: 'ADD_BOARD_REJECTED',

  UPDATE_BOARD_TITLE_REQUEST: 'UPDATE_BOARD_TITLE_REQUEST',
  UPDATE_BOARD_TITLE_FULFILLED: 'UPDATE_BOARD_TITLE_FULFILLED',
  UPDATE_BOARD_TITLE_REJECTED: 'UPDATE_BOARD_TITLE_REJECTED',

  REMOVE_BOARD_REQUEST: 'REMOVE_BOARD_REQUEST',
  REMOVE_BOARD_FULFILLED: 'REMOVE_BOARD_FULFILLED',
  REMOVE_BOARD_REJECTED: 'REMOVE_BOARD_REJECTED',

  GET_NO_BOARD: 'GET_NO_BOARD',
};

const reducer = (state = initState, action) => {
  return produce(state, (draft) => {
    switch (action.type) {
      case BOARD_TYPES.GET_ALL_BOARDS_REQUEST: {
        draft.status.getAllBoardsStatus = LOAD_STATUS.LOADING;
        return draft;
      }
      case BOARD_TYPES.GET_ALL_BOARDS_FULFILLED: {
        draft.status.getAllBoardsStatus = LOAD_STATUS.DONE;
        draft.boards = action.payload;
        return draft;
      }
      case BOARD_TYPES.GET_ALL_BOARDS_REJECTED: {
        draft.status.getAllBoardsStatus = LOAD_STATUS.ERROR;
        draft.message = action.error.message;
        draft.error = action.error.stack;
        return draft;
      }

      case BOARD_TYPES.GET_BOARD_REQUEST: {
        draft.status.getBoardStatus = LOAD_STATUS.LOADING;
        draft.status.getNoBoardStatus = null;
        return draft;
      }
      case BOARD_TYPES.GET_BOARD_FULFILLED: {
        draft.status.getBoardStatus = LOAD_STATUS.DONE;
        draft.board = action.payload;
        return draft;
      }
      case BOARD_TYPES.GET_BOARD_REJECTED: {
        draft.status.getBoardStatus = LOAD_STATUS.ERROR;
        draft.message = action.error.message;
        draft.error = action.error.stack;
        return draft;
      }

      case BOARD_TYPES.ADD_BOARD_REQUEST: {
        draft.status.addBoardStatus = LOAD_STATUS.LOADING;
        return draft;
      }
      case BOARD_TYPES.ADD_BOARD_FULFILLED: {
        draft.status.addBoardStatus = LOAD_STATUS.DONE;
        draft.boards = [action.payload, ...draft.boards];
        draft.board = action.payload;
        return draft;
      }
      case BOARD_TYPES.ADD_BOARD_REJECTED: {
        draft.status.addBoardStatus = LOAD_STATUS.ERROR;
        draft.message = action.error.message;
        draft.error = action.error.stack;
        return draft;
      }

      case BOARD_TYPES.UPDATE_BOARD_TITLE_REQUEST: {
        draft.status.updatedBoardTitleStatus = LOAD_STATUS.LOADING;
        return draft;
      }
      case BOARD_TYPES.UPDATE_BOARD_TITLE_FULFILLED: {
        draft.status.updatedBoardTitleStatus = LOAD_STATUS.DONE;
        const tempBoards = [...draft.boards];
        const index = tempBoards.findIndex((i) => i.id === action.payload.id);
        tempBoards.splice(index, 1, action.payload);
        draft.boards = tempBoards;
        draft.board = action.payload;
        return draft;
      }
      case BOARD_TYPES.UPDATE_BOARD_TITLE_REJECTED: {
        draft.status.updatedBoardTitleStatus = LOAD_STATUS.ERROR;
        draft.message = action.error.message;
        draft.error = action.error.stack;
        return draft;
      }

      case BOARD_TYPES.REMOVE_BOARD_REQUEST: {
        draft.status.removeBoardStatus = LOAD_STATUS.LOADING;
        draft.object.boardId = action.data.id;
        return draft;
      }
      case BOARD_TYPES.REMOVE_BOARD_FULFILLED: {
        draft.status.removeBoardStatus = LOAD_STATUS.DONE;
        const { boardId } = draft.object;
        const tempBoards = [...draft.boards];
        const index = tempBoards.findIndex((i) => i.id === boardId);
        tempBoards.splice(index, 1);
        draft.boards = tempBoards;
        draft.board = null;
        draft.object.boardId = null;
        return draft;
      }
      case BOARD_TYPES.REMOVE_BOARD_REJECTED: {
        draft.status.removeBoardStatus = LOAD_STATUS.ERROR;
        draft.message = action.error.message;
        draft.error = action.error.stack;
        return draft;
      }
      case BOARD_TYPES.GET_NO_BOARD: {
        draft.status = { ...initState.status };
        draft.board = null;
        draft.status.getAllBoardsStatus = LOAD_STATUS.DONE;
        draft.status.getNoBoardStatus = LOAD_STATUS.DONE;
        return draft;
      }
      default:
        return state;
    }
  });
};

export default reducer;
