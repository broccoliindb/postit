import produce from 'immer';
import { LOAD_STATUS } from '../enums';

const initState = {
  status: {
    getAllPostsStatus: null,
    getPostStatus: null,
    addPostStatus: null,
    updatePostStatus: null,
    removePostStatus: null,
    showMessageBoxStatus: null,
    removeAllPostsStatus: null,
    getNoPostStatus: null,
  },
  posts: [],
  post: null,
  error: null,
  message: null,
  object: { postId: null, boardId: null },
};

export const POST_TYPES = {
  GET_ALL_POSTS_REQUEST: 'GET_ALL_POSTS_REQUEST',
  GET_ALL_POSTS_FULFILLED: 'GET_ALL_POSTS_FULFILLED',
  GET_ALL_POSTS_REJECTED: 'GET_ALL_POSTS_REJECTED',

  GET_POST_REQUEST: 'GET_POST_REQUEST',
  GET_POST_FULFILLED: 'GET_POST_FULFILLED',
  GET_POST_REJECTED: 'GET_POST_REJECTED',

  ADD_POST_REQUEST: 'ADD_POST_REQUEST',
  ADD_POST_FULFILLED: 'ADD_POST_FULFILLED',
  ADD_POST_REJECTED: 'ADD_POST_REJECTED',

  UPDATE_POST_REQUEST: 'UPDATE_POST_REQUEST',
  UPDATE_POST_FULFILLED: 'UPDATE_POST_FULFILLED',
  UPDATE_POST_REJECTED: 'UPDATE_POST_REJECTED',

  REMOVE_POST_REQUEST: 'REMOVE_POST_REQUEST',
  REMOVE_POST_FULFILLED: 'REMOVE_POST_FULFILLED',
  REMOVE_POST_REJECTED: 'REMOVE_POST_REJECTED',

  REMOVE_ALL_POSTS_REQUEST: 'REMOVE_ALL_POSTS_REQUEST',
  REMOVE_ALL_POSTS_FULFILLED: 'REMOVE_ALL_POSTS_FULFILLED',
  REMOVE_ALL_POSTS_REJECTED: 'REMOVE_ALL_POSTS_REJECTED',

  SHOW_MESSAGE_BOX: 'SHOW_MESSAGE_BOX',
  CLOSE_MESSAGE_BOX: 'CLOSE_MESSAGE_BOX',
  GET_NO_POST: 'GET_NO_POST',
};

const reducer = (state = initState, action) => {
  return produce(state, (draft) => {
    switch (action.type) {
      case POST_TYPES.GET_ALL_POSTS_REQUEST: {
        draft.status.getAllPostsStatus = LOAD_STATUS.LOADING;
        return draft;
      }
      case POST_TYPES.GET_ALL_POSTS_FULFILLED: {
        draft.status.getAllPostsStatus = LOAD_STATUS.DONE;
        draft.posts = action.payload;
        return draft;
      }
      case POST_TYPES.GET_ALL_POSTS_REJECTED: {
        draft.status.getAllPostsStatus = LOAD_STATUS.ERROR;
        draft.message = action.error.message;
        draft.error = action.error.stack;
        return draft;
      }

      case POST_TYPES.GET_POST_REQUEST: {
        draft.status.getPostStatus = LOAD_STATUS.LOADING;
        return draft;
      }
      case POST_TYPES.GET_POST_FULFILLED: {
        draft.status.getPostStatus = LOAD_STATUS.DONE;
        draft.post = action.payload;
        return draft;
      }
      case POST_TYPES.GET_POST_REJECTED: {
        draft.status.getPostStatus = LOAD_STATUS.ERROR;
        draft.message = action.error.message;
        draft.error = action.error.stack;
        return draft;
      }

      case POST_TYPES.ADD_POST_REQUEST: {
        draft.byHotkey = !!action.data.byHotkey;
        draft.status.addPostStatus = LOAD_STATUS.LOADING;
        return draft;
      }
      case POST_TYPES.ADD_POST_FULFILLED: {
        draft.status.addBoardStatus = null;
        draft.status.addPostStatus = LOAD_STATUS.DONE;
        draft.posts = [...draft.posts, action.payload];
        return draft;
      }
      case POST_TYPES.ADD_POST_REJECTED: {
        draft.status.addBoardStatus = null;
        draft.status.addPostStatus = LOAD_STATUS.ERROR;
        draft.message = action.error.message;
        draft.error = action.error.stack;
        return draft;
      }

      case POST_TYPES.UPDATE_POST_REQUEST: {
        draft.status.updatePostStatus = LOAD_STATUS.LOADING;
        return draft;
      }
      case POST_TYPES.UPDATE_POST_FULFILLED: {
        draft.status.updatePostStatus = LOAD_STATUS.DONE;
        const tempPosts = [...draft.posts];
        const index = tempPosts.findIndex((i) => i.id === action.payload.id);
        tempPosts.splice(index, 1, action.payload);
        draft.posts = tempPosts;
        draft.post = action.payload;
        return draft;
      }
      case POST_TYPES.UPDATE_POST_REJECTED: {
        draft.status.updatePostStatus = LOAD_STATUS.ERROR;
        draft.message = action.error.message;
        draft.error = action.error.stack;
        return draft;
      }

      case POST_TYPES.REMOVE_POST_REQUEST: {
        draft.status.removePostStatus = LOAD_STATUS.LOADING;
        return draft;
      }
      case POST_TYPES.REMOVE_POST_FULFILLED: {
        draft.status.removePostStatus = LOAD_STATUS.DONE;
        const { boardId, postId } = draft.object;
        const tempPosts = [...draft.posts];
        const index = tempPosts.findIndex((i) => i.id === postId && i.boardId === boardId);
        tempPosts.splice(index, 1);
        draft.posts = tempPosts;
        return draft;
      }
      case POST_TYPES.REMOVE_POST_REJECTED: {
        draft.status.removePostStatus = LOAD_STATUS.ERROR;
        draft.message = action.error.message;
        draft.error = action.error.stack;
        return draft;
      }

      case POST_TYPES.REMOVE_ALL_POSTS_REQUEST: {
        draft.status.removeAllPostsStatus = LOAD_STATUS.LOADING;
        return draft;
      }
      case POST_TYPES.REMOVE_ALL_POSTS_FULFILLED: {
        draft.status.removeAllPostsStatus = LOAD_STATUS.DONE;
        draft.posts = [];
        return draft;
      }
      case POST_TYPES.REMOVE_ALL_POSTS_REJECTED: {
        draft.status.removeAllPostsStatus = LOAD_STATUS.ERROR;
        draft.message = action.error.message;
        draft.error = action.error.stack;
        return draft;
      }

      case POST_TYPES.SHOW_MESSAGE_BOX: {
        draft.status.removeAllPostsStatus = null;
        draft.status.showMessageBoxStatus = LOAD_STATUS.LOADING;
        draft.toward = action.data.toward;
        draft.object = action.data.object;
        return draft;
      }
      case POST_TYPES.CLOSE_MESSAGE_BOX: {
        draft.status.showMessageBoxStatus = LOAD_STATUS.DONE;
        return draft;
      }
      case POST_TYPES.GET_NO_POST: {
        draft.status = { ...initState.status };
        draft.posts = [];
        draft.post = null;
        draft.error = null;
        draft.message = null;
        draft.object = { ...initState.object };
        draft.status.getNoPostStatus = LOAD_STATUS.DONE;
        return draft;
      }
      default:
        return state;
    }
  });
};

export default reducer;
