import { combineReducers } from 'redux';
import board, { BOARD_TYPES } from './board';
import post, { POST_TYPES } from './post';
import { LOAD_STATUS } from '../enums';

export const TYPES = {
  ...BOARD_TYPES,
  ...POST_TYPES,
};

export const STATUS = LOAD_STATUS;

const rootReducer = combineReducers({ board, post });

export default rootReducer;
