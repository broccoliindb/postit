import { createGlobalStyle } from 'styled-components';
import reset from 'styled-reset';
import '../rootVariables.css';
import '../rootStyle.css';

const GlobalStyle = createGlobalStyle`
  ${reset}
  * {
    box-sizing: border-box;
  }
  [placeholder]{
    text-overflow:ellipsis;
  }
  body {
    font-family: 'Nanum Gothic';
  }
`;

export default GlobalStyle;
