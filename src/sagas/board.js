import { all, call, put, takeLatest, fork, debounce } from 'redux-saga/effects';
import { TYPES } from '../reducers/rootReducer';
import { getBoard, setBoard, deleteBoard, clearBoard, updateBoard, getAllBoards } from '../db';

// APIs
const getAllBoardsApi = async () => {
  const result = await getAllBoards();
  return result;
};
const getBoardApi = async (boardId) => {
  const result = await getBoard(boardId);
  return result;
};
const addBoardApi = async (board) => {
  await setBoard(board);
  const result = await getBoard(board.id);
  return result;
};
const updateBoardApi = async (data) => {
  const { id, title } = data;
  const result = await updateBoard(id, title);
  return result;
};
const removeBoardApi = async (data) => {
  const { id } = data;
  const result = await deleteBoard(id);
  return result;
};

// REQUESTS
function* getAllBoardsRequest() {
  try {
    const boards = yield call(getAllBoardsApi);
    yield put({ type: TYPES.GET_ALL_BOARDS_FULFILLED, payload: boards });
  } catch (error) {
    if (error) {
      console.error(error);
      yield put({ type: TYPES.GET_ALL_BOARDS_REJECTED, error });
    }
  }
}
function* getBoardRequest(action) {
  try {
    const board = yield call(getBoardApi, action.data);
    yield put({ type: TYPES.GET_BOARD_FULFILLED, payload: board });
  } catch (error) {
    if (error) {
      console.error(error);
      yield put({ type: TYPES.GET_BOARD_REJECTED, error });
    }
  }
}
function* addBoardRequest(action) {
  try {
    const board = yield call(addBoardApi, action.data);
    yield put({ type: TYPES.ADD_BOARD_FULFILLED, payload: board });
  } catch (error) {
    if (error) {
      console.error(error);
      yield put({ type: TYPES.ADD_BOARD_REJECTED, error });
    }
  }
}
function* updateBoardRequest(action) {
  try {
    const board = yield call(updateBoardApi, action.data);
    yield put({ type: TYPES.UPDATE_BOARD_TITLE_FULFILLED, payload: board });
  } catch (error) {
    if (error) {
      console.error(error);
      yield put({ type: TYPES.UPDATE_BOARD_TITLE_REJECTED, error });
    }
  }
}
function* removeBoardRequest(action) {
  try {
    const board = yield call(removeBoardApi, action.data);
    yield put({ type: TYPES.REMOVE_BOARD_FULFILLED, payload: board });
  } catch (error) {
    if (error) {
      console.error(error);
      yield put({ type: TYPES.REMOVE_BOARD_REJECTED, error });
    }
  }
}

// WATCHERS

function* watchGetAllBoards() {
  yield takeLatest(TYPES.GET_ALL_BOARDS_REQUEST, getAllBoardsRequest);
}
function* watchGetBoard() {
  yield takeLatest(TYPES.GET_BOARD_REQUEST, getBoardRequest);
}
function* watchAddBoard() {
  yield takeLatest(TYPES.ADD_BOARD_REQUEST, addBoardRequest);
}
function* watchRemoveBoard() {
  yield takeLatest(TYPES.REMOVE_BOARD_REQUEST, removeBoardRequest);
}
export default function* boardSagas() {
  yield all([
    fork(watchGetAllBoards),
    fork(watchGetBoard),
    fork(watchAddBoard),
    debounce(500, TYPES.UPDATE_BOARD_TITLE_REQUEST, updateBoardRequest),
    fork(watchRemoveBoard),
  ]);
}
