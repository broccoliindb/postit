import { all, fork } from 'redux-saga/effects';
import boardSagas from './board';
import postSagas from './post';

export default function* rootSaga() {
  yield all([fork(boardSagas), fork(postSagas)]);
}
