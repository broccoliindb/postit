import { all, call, put, takeLatest, fork, debounce } from 'redux-saga/effects';
import { TYPES } from '../reducers/rootReducer';
import { getPost, setPost, getAllPosts, updatePost, deletePost, deleteAllPosts } from '../db';

// APIs
const getAllPostsApi = async (boardId) => {
  const result = await getAllPosts(boardId);
  return result;
};
const getPostApi = async (data) => {
  const { boardId, id } = data;
  const result = await getPost(boardId, id);
  return result;
};
const addPostApi = async (post) => {
  const { boardId, id } = post;
  await setPost(post);
  const result = await getPost(boardId, id);
  return result;
};
const updatePostApi = async (data) => {
  const { boardId, id, title, content } = data;
  const result = await updatePost(boardId, id, title, content);
  return result;
};
const removePostApi = async (data) => {
  const { boardId, id } = data;
  const result = await deletePost(boardId, id);
  return result;
};
const removeAllPostsApi = async (data) => {
  const { boardId } = data;
  const result = await deleteAllPosts(boardId);
  return result;
};

// REQUESTS
function* getAllPostsRequest(action) {
  try {
    const posts = yield call(getAllPostsApi, action.data);
    yield put({ type: TYPES.GET_ALL_POSTS_FULFILLED, payload: posts });
  } catch (error) {
    if (error) {
      console.error(error);
      yield put({ type: TYPES.GET_ALL_POSTS_REJECTED, error });
    }
  }
}
function* getPostRequest(action) {
  try {
    const post = yield call(getPostApi, action.data);
    yield put({ type: TYPES.GET_POST_FULFILLED, payload: post });
  } catch (error) {
    if (error) {
      console.error(error);
      yield put({ type: TYPES.GET_POST_REJECTED, error });
    }
  }
}
function* addPostRequest(action) {
  try {
    const post = yield call(addPostApi, action.data);
    yield put({ type: TYPES.ADD_POST_FULFILLED, payload: post });
  } catch (error) {
    if (error) {
      console.error(error);
      yield put({ type: TYPES.ADD_POST_REJECTED, error });
    }
  }
}
function* updatePostRequest(action) {
  try {
    const post = yield call(updatePostApi, action.data);
    yield put({ type: TYPES.UPDATE_POST_FULFILLED, payload: post });
  } catch (error) {
    if (error) {
      console.error(error);
      yield put({ type: TYPES.UPDATE_POST_REJECTED, error });
    }
  }
}
function* removePostRequest(action) {
  try {
    const post = yield call(removePostApi, action.data);
    yield put({ type: TYPES.REMOVE_POST_FULFILLED, payload: post });
  } catch (error) {
    if (error) {
      console.error(error);
      yield put({ type: TYPES.REMOVE_POST_REJECTED, error });
    }
  }
}
function* removeAllPostsRequest(action) {
  try {
    yield call(removeAllPostsApi, action.data);
    yield put({ type: TYPES.REMOVE_ALL_POSTS_FULFILLED });
  } catch (error) {
    if (error) {
      console.error(error);
      yield put({ type: TYPES.REMOVE_ALL_POSTS_REJECTED, error });
    }
  }
}
// WATCHERS
function* watchGetAllPosts() {
  yield takeLatest(TYPES.GET_ALL_POSTS_REQUEST, getAllPostsRequest);
}
function* watchGetPost() {
  yield takeLatest(TYPES.GET_POST_REQUEST, getPostRequest);
}
function* watchAddPost() {
  yield takeLatest(TYPES.ADD_POST_REQUEST, addPostRequest);
}
function* watchRemovePost() {
  yield takeLatest(TYPES.REMOVE_POST_REQUEST, removePostRequest);
}
function* watchRemoveAllPosts() {
  yield takeLatest(TYPES.REMOVE_ALL_POSTS_REQUEST, removeAllPostsRequest);
}

export default function* boardSagas() {
  yield all([
    fork(watchGetAllPosts),
    fork(watchGetPost),
    fork(watchAddPost),
    debounce(500, TYPES.UPDATE_POST_REQUEST, updatePostRequest),
    fork(watchRemovePost),
    fork(watchRemoveAllPosts),
  ]);
}
