import { openDB } from 'idb/with-async-ittr';

const dbPromise = openDB('postitDB', 1, {
  upgrade(db) {
    const boardStore = db.createObjectStore('boards', {
      keyPath: 'id',
      autoIncrement: true,
    });
    boardStore.createIndex('id', 'id');
    const postStore = db.createObjectStore('posts', {
      keyPath: ['boardId', 'id'],
    });
    postStore.createIndex('boardId', 'boardId');
  },
});

// Boards
export const getBoard = async (key) => {
  return (await dbPromise).get('boards', key);
};
export const setBoard = async (val) => {
  return (await dbPromise).put('boards', val);
};
export const deleteBoard = async (key) => {
  return (await dbPromise).delete('boards', key);
};
export const clearBoard = async () => {
  return (await dbPromise).clear('boards');
};
export const updateBoard = async (id, title) => {
  const tx = (await dbPromise).transaction('boards', 'readwrite');
  const index = tx.store.index('id');
  for await (const cursor of index.iterate(id)) {
    const board = { ...cursor.value };
    board.title = title;
    cursor.update(board);
  }
  await tx.done;
  return getBoard(id);
};
export const getAllBoards = async () => {
  const tx = (await dbPromise).transaction('boards', 'readonly');
  const index = tx.store.index('id');
  const result = [];
  for await (const cursor of index.iterate(null, 'prev')) {
    result.push(cursor.value);
  }
  await tx.done;
  return Promise.resolve(result);
};

// Posts
export const getPost = async (boardId, id) => {
  return (await dbPromise).get('posts', [boardId, id]);
};
export const setPost = async (val) => {
  return (await dbPromise).add('posts', val);
};
export const getAllPosts = async (boardId) => {
  return (await dbPromise).getAllFromIndex('posts', 'boardId', boardId);
};
export const updatePost = async (boardId, id, title = '', content = '') => {
  const tx = (await dbPromise).transaction('posts', 'readwrite');
  const index = tx.store.index('boardId', boardId);
  for await (const cursor of index.iterate(boardId)) {
    const post = { ...cursor.value };
    if (post.id === id) {
      post.title = title;
      post.content = content;
      cursor.update(post);
    }
  }
  await tx.done;
  return getPost(boardId, id);
};

export const deletePost = async (boardId, id) => {
  return (await dbPromise).delete('posts', [boardId, id]);
};

export const deleteAllPosts = async (boardId) => {
  const tx = (await dbPromise).transaction('posts', 'readwrite');
  const index = tx.store.index('boardId', boardId);
  for await (const cursor of index.iterate(boardId)) {
    const post = { ...cursor.value };
    cursor.delete(post);
  }
  await tx.done;
};
