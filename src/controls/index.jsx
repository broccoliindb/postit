import React from 'react';
import styled from 'styled-components';

const Grid = styled.div`
  margin: 0.2rem;
  display: grid;
  grid-template-columns: ${(props) => (props.columns ? `repeat(${props.columns}, 1fr)}` : 'repeat(auto-fit, 1fr)}')};
  grid-gap: 0.2rem;
`;
const ButtonWrapper = styled.button`
  border-radius: 5px;
  padding: ${(props) => (props.padding ? `${props.padding}` : '0.2rem 1rem;')};
  outline: none;
  font-weight: bold;
  border: ${(props) => (props.disabled ? '2px solid var(--greyColor3)' : '2px solid var(--everGreenColor)')};
  background-color: ${(props) => (props.isFilled ? 'var(--everGreenColor)' : props.disabled ? 'var(--greyColor3)' : 'var(--whiteColor)')};
  color: ${(props) => (props.isFilled ? 'var(--whiteColor)' : props.disabled ? 'var(--whiteColor)' : 'var(--everGreenColor)')};
  font-size: 1rem;
  &:hover {
    background-color: ${(props) => (props.disabled ? 'var(--greyColor3)' : 'var(--greyColor6)')};
    color: var(--whiteColor);
    border: ${(props) => (props.disabled ? '2px solid var(--greyColor3)' : '2px solid var(--greyColor6)')};
  }
  cursor: ${(props) => (props.disabled ? 'default' : 'pointer')};
`;
const NotFound = styled.div`
  padding: 2rem;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100%;
  font-size: 3rem;
  background-color: ${(props) => (props.isPost ? 'var(--everGreenColor)' : 'var(--greyColor2)')};
  color: ${(props) => (props.isPost ? 'var(--whiteColor)' : 'var(--everGreenColor)')};
  line-height: 4rem;
  @media only screen and (max-width: 600px) {
    font-size: 2rem;
    font-weight: bold;
  }
`;

const InputWrapper = styled.input`
  padding: 0.5rem 1rem;
  outline: none;
  border: none;
  color: var(--blackColor);
  line-height: ${(props) => (props.fontSize ? `${props.fontSize}` : '1rem')};
  font-size: ${(props) => (props.fontSize ? `${props.fontSize}` : '1rem')};
  height: ${(props) => (props.height ? `${props.height}` : '1rem')};
  cursor: ${(props) => (props.isDragEnable ? 'move' : 'text')};
`;

const TextareaWrapper = styled.textarea`
  min-height: 5rem;
  cursor: ${(props) => (props.isDragEnable ? 'move' : 'text')}; ;
`;

const gridBox = (props, ref) => {
  const { children, ...commonProps } = props;
  return (
    <Grid {...commonProps} ref={ref}>
      {children}
    </Grid>
  );
};
const button = (props, ref) => {
  const { children, ...commonProps } = props;
  return (
    <ButtonWrapper {...commonProps} ref={ref}>
      {children}
    </ButtonWrapper>
  );
};
const nothing = (props, ref) => {
  const { children, ...commonProps } = props;
  return (
    <NotFound {...commonProps} ref={ref}>
      {children}
    </NotFound>
  );
};
const input = (props, ref) => {
  const { children, ...commonProps } = props;
  return (
    <InputWrapper {...commonProps} ref={ref}>
      {children}
    </InputWrapper>
  );
};
const textarea = (props, ref) => {
  const { children, ...commonProps } = props;
  return (
    <TextareaWrapper {...commonProps} ref={ref}>
      {children}
    </TextareaWrapper>
  );
};

export const GridBox = React.forwardRef(gridBox);
export const Button = React.forwardRef(button);
export const Nothing = React.forwardRef(nothing);
export const Input = React.forwardRef(input);
export const Textarea = React.forwardRef(textarea);
