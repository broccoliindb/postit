import React, { useCallback, useState, useRef, useEffect, memo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TYPES, STATUS } from '../reducers/rootReducer';
import styled from 'styled-components';
import Post from './Post';
import { Nothing } from '../controls';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { addIcon, CustomFAIcon } from '../fontawesome';
import useDebounce from '../hooks/useDebounce';

addIcon(faPlus);

const Container = styled.div`
  position: relative;
  background-color: var(--everGreenColor);
  @media only screen and (max-width: 600px) {
    overflow: auto;
    display: grid;
    grid-template-columns: 1fr;
    grid-template-rows: repeat(auto-fill, 1fr);
    grid-auto-rows: 1fr;
    grid-gap: 1rem;
    background-color: var(--whiteColor);
  }
`;
const Adder = styled.div`
  display: none;
  visibility: collapse;
  @media only screen and (max-width: 600px) {
    display: block;
    position: fixed;
    visibility: visible;
    bottom: 6rem;
    right: 0;
    padding: 1rem;
    background-color: var(--brightBlueColor);
    color: var(--whiteColor);
    border-radius: 5px;
    padding: 0.5rem;
    margin-right: 1rem;
    z-index: 30;
    box-shadow: rgb(0 0 0 / 20%) 0px 2px 10px;
  }
`;

const Posts = () => {
  const { board, status: boardStatus } = useSelector((state) => state.board);
  const { posts, status: postStatus } = useSelector((state) => state.post);
  const { getBoardStatus, addBoardStatus } = boardStatus;
  const { getAllPostsStatus, addPostStatus } = postStatus;
  const boardId = board?.id;
  const postRef = useRef(null);
  const dispatch = useDispatch();
  const [touchStack, setTouchStack] = useState([]);

  const onCreatePost = useCallback(
    (e) => {
      if (postRef.current) {
        const target = e.target;
        if (target.closest('.post')) return;
      }
      if (!board) return;
      dispatch({
        type: TYPES.ADD_POST_REQUEST,
        data: { boardId, id: Date.now(), title: '제목없음', content: '' },
      });
    },
    [boardId, posts],
  );
  const onSetOrder = useCallback(
    (postId) => {
      const index = touchStack.findIndex((i) => i === postId);
      if (index === -1) {
        const copy = [...touchStack];
        setTouchStack([...copy, postId]);
      } else {
        const copy = [...touchStack];
        copy.splice(index, 1);
        setTouchStack([...copy, postId]);
      }
    },
    [touchStack],
  );
  const createPost = useDebounce(onCreatePost, 200);
  useEffect(() => {
    if ((getBoardStatus === STATUS.DONE && board) || (addPostStatus === STATUS.DONE && board)) {
      dispatch({ type: TYPES.GET_ALL_POSTS_REQUEST, data: board.id });
    }
  }, [status, boardStatus]);
  return (
    <Container onDoubleClick={createPost}>
      {(getAllPostsStatus === STATUS.DONE || addPostStatus === STATUS.DONE) && posts.length > 0 && (
        <>
          {posts.map((post) => {
            const { id: postId } = post;
            return <Post ref={postRef} key={postId} boardId={boardId} post={post} onSetOrder={onSetOrder} orders={touchStack} />;
          })}
        </>
      )}
      {(getAllPostsStatus === STATUS.DONE || addPostStatus === STATUS.DONE || addBoardStatus === STATUS.DONE) && posts.length === 0 && (
        <Nothing isPost>
          <span>더블클릭나 단축키(alt+ctrl+n), 스마트폰은 파란색 + 버튼으로 메모를 추가하세요.😎</span>
        </Nothing>
      )}
      <Adder onClick={createPost}>
        <CustomFAIcon icon={faPlus} />
      </Adder>
    </Container>
  );
};

export default memo(Posts);
