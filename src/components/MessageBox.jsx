import React, { memo } from 'react';
import styled from 'styled-components';
import useDebounce from '../hooks/useDebounce';
import { Button, GridBox } from '../controls';
import PropTypes from 'prop-types';

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  display: grid;
  place-items: center;
  background-color: var(--whiteBaseTransparnetBg);
  z-index: 2000;
`;
const Box = styled.div`
  display: grid;
  grid-template-rows: auto 1fr auto;
  grid-gap: 1rem;
  padding: 1rem;
  background-color: var(--whiteColor);
  box-shadow: rgb(0 0 0 / 20%) 0px 2px 10px;
  border-radius: 15px;
`;
const H4 = styled.h4`
  font-size: 1.5rem;
  text-align: center;
  font-weight: bold;
`;
const MessageBox = ({ onCancel, onOK, toward }) => {
  const title = toward === 'POST' ? '메모삭제' : '보드삭제';
  const message = toward === 'POST' ? '삭제하시겠습니까?' : '보드를 삭제하면 해당보드의 포스트 모두 삭제됩니다. 삭제하시겠습니까?';
  const cancel = useDebounce(onCancel, 300);
  const ok = useDebounce(onOK, 300);
  return (
    <Container>
      <Box>
        <H4>{title}</H4>
        <p>{message}</p>
        <GridBox columns={2} style={{ gridTemplateColumns: '1fr auto' }}>
          <Button style={{ width: 'max-content', justifySelf: 'end' }} onClick={cancel}>
            취소
          </Button>
          <Button style={{ width: 'max-content', color: 'var(--whiteColor)' }} isFilled onClick={ok}>
            확인
          </Button>
        </GridBox>
      </Box>
    </Container>
  );
};

MessageBox.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onOK: PropTypes.func.isRequired,
  toward: PropTypes.string.isRequired,
};

export default memo(MessageBox);
