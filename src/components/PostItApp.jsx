import React, { useCallback, useState, useEffect, memo } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import Board from './Board';
import BoardList from './BoardList';
import MessageBox from './MessageBox';
import { STATUS, TYPES } from '../reducers/rootReducer';
import useHotkeys from '../hooks/useHotkeys';
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { addIcon, CustomFAIcon } from '../fontawesome';
import useDebounce from '../hooks/useDebounce';

addIcon(faBars);

const Container = styled.div`
  position: relative;
  display: grid;
  grid-template-columns: repeat(24, 1fr);
  height: 100vh;
`;
const Menu = styled.div`
  display: none;
  visibility: collapse;
  @media only screen and (max-width: 600px) {
    display: block;
    position: fixed;
    visibility: visible;
    bottom: 3rem;
    right: 0;
    padding: 1rem;
    background-color: var(--brightPinkColor);
    color: var(--whiteColor);
    border-radius: 5px;
    padding: 0.5rem;
    margin-right: 1rem;
    z-index: 30;
    box-shadow: rgb(0 0 0 / 20%) 0px 2px 10px;
  }
`;

const PostItApp = () => {
  const dispatch = useDispatch();
  // 모바일을 위한 상태
  const [isShowBoardList, setIsShowBoardList] = useState(false);
  const [isAddBoard, setisAddBoard] = useState(false);
  useHotkeys('alt+ctrl+n', () => {
    if (!board) {
      //새로운 보드 열어서 메모장 만든다.
      dispatch({
        type: TYPES.ADD_BOARD_REQUEST,
        data: {
          id: Date.now(),
          title: '제목없음',
        },
      });
      setisAddBoard(true);
    } else {
      //있는 보드장에 메모장 만든다.
      const boardId = board.id;
      dispatch({ type: TYPES.ADD_POST_REQUEST, data: { boardId, id: Date.now(), title: '제목없음', content: '', byHotkey: true } });
    }
  });
  const { board, status: boardStatus } = useSelector((state) => state.board);
  const { addBoardStatus } = boardStatus;
  const { status: postStatus, toward, object, posts } = useSelector((state) => state.post);
  const { showMessageBoxStatus, removeAllPostsStatus } = postStatus;
  const { postId, boardId } = object;
  const [showMessageBox, setShowMessageBox] = useState(false);
  const onOK = useCallback(() => {
    if (postId) {
      dispatch({ type: TYPES.REMOVE_POST_REQUEST, data: { boardId, id: postId } });
    } else {
      // 해당 보드의 포스트가 있느지 파악 있으면 포스트먼저삭제하고 보드 삭제
      if (posts.length > 0) {
        dispatch({ type: TYPES.REMOVE_ALL_POSTS_REQUEST, data: { boardId } });
      } else {
        dispatch({
          type: TYPES.REMOVE_BOARD_REQUEST,
          data: {
            id: boardId,
          },
        });
      }
    }
    dispatch({ type: TYPES.CLOSE_MESSAGE_BOX });
  }, [postId, boardId]);
  const onCancel = useCallback(() => {
    dispatch({ type: TYPES.CLOSE_MESSAGE_BOX });
    setShowMessageBox(false);
  }, []);

  // 모바일에서 보드리스트를 보이게해주는 함수
  const onShowBoardList = useCallback(() => {
    setIsShowBoardList((prev) => !prev);
  }, []);
  const showBoards = useDebounce(onShowBoardList, 200);
  useEffect(() => {
    if (showMessageBoxStatus === STATUS.LOADING) {
      setShowMessageBox(true);
    }
    if (showMessageBoxStatus === STATUS.DONE) {
      setShowMessageBox(false);
    }
  }, [showMessageBoxStatus]);
  useEffect(() => {
    if (addBoardStatus === STATUS.DONE && board && isAddBoard) {
      dispatch({
        type: TYPES.ADD_POST_REQUEST,
        data: { boardId: board.id, id: Date.now(), title: '제목없음', content: '', byHotkey: true },
      });
      setisAddBoard(false);
    }
  }, [addBoardStatus, board, isAddBoard]);
  useEffect(() => {
    if (removeAllPostsStatus === STATUS.DONE && boardId) {
      dispatch({
        type: TYPES.REMOVE_BOARD_REQUEST,
        data: {
          id: boardId,
        },
      });
    }
  }, [removeAllPostsStatus, boardId]);
  return (
    <Container>
      <BoardList isShowBoardList={isShowBoardList} showBoards={showBoards} />
      <Board />
      {showMessageBox && <MessageBox onCancel={onCancel} onOK={onOK} toward={toward} />}
      <Menu onClick={showBoards}>
        <CustomFAIcon icon={faBars} />
      </Menu>
    </Container>
  );
};

export default memo(PostItApp);
