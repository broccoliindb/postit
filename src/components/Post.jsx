import React, { useCallback, useState, useRef, useEffect, memo, forwardRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { STATUS, TYPES } from '../reducers/rootReducer';
import styled from 'styled-components';
import useDrag from '../hooks/useDrag';
import { GridBox } from '../controls';
import PropTypes from 'prop-types';

const Container = styled.div`
  resize: ${(props) => (props.isMinimumPost || props.isDragEnable ? 'none' : 'both')};
  overflow: auto;
  height: ${(props) => (props.isMinimumPost ? 'auto !important' : 'auto')};
  z-index: ${(props) => (props.isDragging ? 1000 : props.order)};
  padding: 0 1rem 0.5rem 1rem;
  background-color: var(--yellowLightColor);
  box-shadow: rgb(0 0 0 / 20%) 0px 2px 10px;
  @media only screen and (max-width: 600px) {
    display: block !important;
    position: relative !important;
    padding: 0;
    margin: 0;
    left: 0 !important;
    right: 0 !important;
    top: 0 !important;
    box-shadow: rgb(0 0 0 / 20%) 0px 2px 10px;
    resize: none;
  }
`;
const InnerContainer = styled.div`
  display: grid;
  grid-template-columns: minmax(3rem, 1fr);
  grid-template-rows: 2rem auto;
  min-height: 3rem;
  height: 100%;
  cursor: none;
`;
const PostHeader = styled.div`
  display: grid;
  grid-template-columns: 1fr auto;
  grid-template-rows: 1fr;
`;
const TitleInput = styled.input`
  background-color: var(--yellowLightColor);
  padding-left: 0.5rem;
  outline: none;
  border: none;
  height: 2rem;
  min-width: 3rem;
  cursor: ${(props) => (props.isDragEnable ? 'move' : 'text')};
`;
const Buttons = styled(GridBox)`
  grid-template-columns: repeat(2, 1.5rem);
  @media only screen and (max-width: 600px) {
    grid-template-columns: 1.5rem;
    button {
      &:nth-child(1) {
        display: none;
        visibility: collapse;
      }
    }
  }
`;
const Button = styled.button`
  display: grid;
  place-items: center;
  cursor: pointer;
  font-weight: bold;
  border: none;
  color: var(--greyColor6);
  background-color: var(--yellowColor);
  border-radius: 5px;
  &:hover {
    color: var(--redColor);
    background-color: var(--greyColor4);
  }
`;
const Textarea = styled.textarea`
  outline: none;
  border: none;
  resize: none;
  min-width: 6.5rem;
  min-height: 100%;
  padding: 0.5rem;
  background-color: var(--yellowLightColor);
  cursor: ${(props) => (props.isDragEnable ? 'move' : 'text')};
  @media only screen and (max-width: 600px) {
    height: 8rem;
  }
`;

const Post = forwardRef(({ boardId, post, onSetOrder, orders }, ref) => {
  const dispatch = useDispatch();
  const { status: postStatus, byHotkey } = useSelector((state) => state.post);
  const { addPostStatus } = postStatus;
  const titleRef = useRef(null);
  const contentRef = useRef(null);
  const id = post?.id;
  const content = post?.content;
  const title = post?.title;
  const [postTitle, setPostTitle] = useState(title || '');
  const [postContent, setPostContent] = useState(content || '');
  const [isTitleEditMode, setIsTitleEditMode] = useState(true);
  const [isContentEditMode, setIsContentEditMode] = useState(true);
  const [isMinimumPost, setIsMinimumPost] = useState(false);
  const order = orders.findIndex((i) => i === id) + 5;
  const { innerHeight, innerWidth } = window;
  const startLoc = {
    x: innerWidth / 2 - 320 + Math.ceil(Math.random() * 50),
    y: innerHeight / 2 - 250 + Math.ceil(Math.random() * 50),
  };
  const { isDragging, location, handleMouseDown, handleMouseMove, handleMouseUp, handleMouseLeave } = useDrag(
    startLoc,
    setIsContentEditMode,
  );
  const onEditTitle = useCallback(() => {
    setIsTitleEditMode(true);
  }, []);
  const onLeaveTitleInput = useCallback(() => {
    setIsTitleEditMode(false);
  }, []);
  const onChangeTitle = useCallback(
    (e) => {
      setPostTitle(e.target.value);
      if (e.target.value) {
        dispatch({ type: TYPES.UPDATE_POST_REQUEST, data: { boardId, id, title: e.target.value, content: postContent } });
      }
    },
    [id, boardId, postContent],
  );
  const onChangeContent = useCallback(
    (e) => {
      setPostContent(e.target.value);
      if (e.target.value) {
        dispatch({ type: TYPES.UPDATE_POST_REQUEST, data: { boardId, id, title: postTitle, content: e.target.value } });
      }
    },
    [id, boardId, postTitle],
  );

  const onEditBoardDone = useCallback((e) => {
    if (e.key === 'Enter') {
      setIsContentEditMode(false);
      setIsTitleEditMode(false);
    }
  }, []);

  const onRemovePost = useCallback(() => {
    dispatch({
      type: TYPES.SHOW_MESSAGE_BOX,
      data: { toward: 'POST', object: { boardId, postId: id } },
    });
  }, [id, boardId]);

  const onMinimumPost = useCallback(() => {
    setIsMinimumPost((prev) => !prev);
  }, []);
  useEffect(() => {
    if (isTitleEditMode || isContentEditMode) {
      onSetOrder(id);
    }
  }, [isTitleEditMode, isContentEditMode]);
  useEffect(() => {
    if (addPostStatus === STATUS.DONE && byHotkey && titleRef.current) {
      setIsTitleEditMode(true);
      setIsContentEditMode(false);
      titleRef.current.focus();
    }
  }, [addPostStatus, byHotkey]);
  useEffect(() => {
    if (isMinimumPost && isContentEditMode) {
      setIsContentEditMode(false);
    }
  }, [isMinimumPost]);
  return (
    <>
      {post && post.boardId === boardId && (
        <Container
          className="post"
          ref={ref}
          style={{ ...location }}
          isDragging={isDragging}
          isDragEnable={!isContentEditMode}
          isMinimumPost={isMinimumPost}
          id={id}
          order={order}
        >
          <InnerContainer>
            <PostHeader>
              <TitleInput
                onBlur={onLeaveTitleInput}
                readOnly={!isTitleEditMode}
                isDragEnable={isMinimumPost}
                ref={titleRef}
                value={postTitle}
                onChange={onChangeTitle}
                onClick={onEditTitle}
                onKeyDown={onEditBoardDone}
                onMouseDown={handleMouseDown}
                onMouseMove={handleMouseMove}
                onMouseUp={handleMouseUp}
                onMouseLeave={handleMouseLeave}
              />
              <Buttons>
                <Button onClick={onMinimumPost}>{isMinimumPost ? <span>+</span> : <span>_</span>}</Button>
                <Button onClick={onRemovePost}>x</Button>
              </Buttons>
            </PostHeader>
            {!isMinimumPost && (
              <Textarea
                readOnly={!isContentEditMode}
                isDragEnable={!isContentEditMode}
                ref={contentRef}
                value={postContent}
                rows={10}
                onChange={onChangeContent}
                onKeyDown={onEditBoardDone}
                onMouseDown={handleMouseDown}
                onMouseMove={handleMouseMove}
                onMouseUp={handleMouseUp}
                onMouseLeave={handleMouseLeave}
              />
            )}
          </InnerContainer>
        </Container>
      )}
    </>
  );
});
Post.displayName = 'Post';

Post.propTypes = {
  boardId: PropTypes.number.isRequired,
  post: PropTypes.shape({
    boardId: PropTypes.number.isRequired,
    id: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
  }),
  onSetOrder: PropTypes.func.isRequired,
  orders: PropTypes.array.isRequired,
};

export default memo(Post);
