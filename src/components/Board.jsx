import React, { useState, useCallback, useRef, useEffect, memo } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import Posts from './Posts';
import { STATUS, TYPES } from '../reducers/rootReducer';
import useDebounce from '../hooks/useDebounce';
import { Nothing, Input, GridBox } from '../controls';

const Container = styled.main`
  position: relative;
  grid-column: 6 / -1;
  background-color: var(--greyColor2);
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 1fr;
  @media only screen and (max-width: 600px) {
    grid-column: 1 / -1;
  }
  @media only screen and (min-width: 1200px) {
    grid-column: 3/-1;
  }
`;

const GridBoxWrapper = styled(GridBox)`
  grid-template-columns: 100%;
  grid-template-rows: auto 1fr;
`;

const Board = () => {
  const titleRef = useRef(null);
  const dispatch = useDispatch();
  const { board, status } = useSelector((state) => state.board);
  const { getBoardStatus, addBoardStatus, getAllBoardsStatus } = status;
  const title = board?.title;
  const id = board?.id;
  const [boardTitle, setBoardTitle] = useState(title || '');
  const [isTitleEditMode, setIsTitleEditMode] = useState(false);

  const onChangeBoardTitle = useCallback(
    (e) => {
      setBoardTitle(e.target.value);
      if (id) {
        dispatch({
          type: TYPES.UPDATE_BOARD_TITLE_REQUEST,
          data: {
            id,
            title: e.target.value,
          },
        });
      }
    },
    [id],
  );
  const onEditTitle = useCallback(() => {
    setIsTitleEditMode(true);
  }, []);
  const onFocusOut = useCallback((e) => {
    if (titleRef.current) {
      if (!titleRef.current.contains(e.target)) {
        setIsTitleEditMode(false);
      }
    }
  }, []);
  const onEditBoardTitleDone = useCallback(
    (e) => {
      if (e.key === 'Enter' && boardTitle) {
        setIsTitleEditMode(false);
      }
    },
    [boardTitle],
  );
  const editBoardTitleDone = useDebounce(onEditBoardTitleDone, 300);
  useEffect(() => {
    if ((getBoardStatus === STATUS.DONE && title) || (addBoardStatus === STATUS.DONE && board)) {
      setBoardTitle(title);
    }
  }, [title, getBoardStatus]);
  useEffect(() => {
    if (isTitleEditMode && titleRef.current) {
      titleRef.current.focus();
    }
  }, [isTitleEditMode]);
  useEffect(() => {
    if (addBoardStatus === STATUS.DONE && titleRef.current) {
      setIsTitleEditMode(true);
    }
  }, [addBoardStatus]);
  return (
    <Container onClick={onFocusOut} board={!!board}>
      {board && (
        <GridBoxWrapper>
          <Input
            fontSize="1.5rem"
            height="3.5rem"
            readOnly={!isTitleEditMode}
            ref={titleRef}
            value={boardTitle}
            style={{ color: 'var(--everGreenColor)' }}
            onClick={onEditTitle}
            onChange={onChangeBoardTitle}
            onKeyDown={editBoardTitleDone}
            placeholder="메모보드의 제목을 작성하세요..."
          />
          <Posts />
        </GridBoxWrapper>
      )}
      {(getBoardStatus === STATUS.DONE || addBoardStatus === STATUS.DONE || getAllBoardsStatus === STATUS.DONE) && !board && (
        <Nothing>
          <span>메모보드를 선택해주세요.😎</span>
        </Nothing>
      )}
    </Container>
  );
};

export default memo(Board);
