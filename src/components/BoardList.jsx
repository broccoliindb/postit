import React, { useCallback, useEffect, useState, memo, useRef } from 'react';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';
import { STATUS, TYPES } from '../reducers/rootReducer';
import useDebounce from '../hooks/useDebounce';
import { addIcon, CustomFAIcon } from '../fontawesome';
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';
import { GridBox, Button } from '../controls';
import PropTypes from 'prop-types';

addIcon(faMinus, faPlus);

const Container = styled.aside`
  grid-column: 1 / 6;
  background-color: var(--whiteColor);
  @media only screen and (max-width: 600px) {
    display: grid;
    grid-template-rows: 1fr auto;
    grid-column: 1/-1;
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    transform: ${(props) => (props.isShowBoardList ? 'translateX(0rem)' : 'translateX(-30rem)')};
    z-index: 20;
  }
  @media only screen and (min-width: 1200px) {
    grid-column: 1/3;
  }
`;
const Boards = styled.ul`
  overflow: auto;
`;
const BoardItem = styled.li`
  background-color: ${(props) => (props.selected ? 'var(--everGreenColor)' : 'transperant')};
  padding: 0.5rem;
  box-shadow: ${(props) => (props.selected ? 'rgb(0 0 0 / 20%) 0px 2px 50px;' : 'none')};
  color: ${(props) => (props.selected ? 'var(--whiteColor)' : 'var(--greyColor6)')};
  font-weight: ${(props) => (props.selected ? 'bold' : 'normal')};
  &:hover {
    background-color: var(--greyColor6);
    color: var(--whiteColor);
  }
  height: 2rem;
  cursor: pointer;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

const GridBoxWrapper = styled(GridBox)`
  @media only screen and (min-width: 1200px) {
    button: {
      align-content: end;
    }
  }
`;

const BoardList = ({ isShowBoardList, showBoards }) => {
  const dispatch = useDispatch();
  const boardListRef = useRef(null);
  const buttonsRef = useRef(null);
  const { boards, board, status } = useSelector((state) => state.board);
  const { getAllBoardsStatus, removeBoardStatus } = status;
  const [isSelected, setIsSelected] = useState(false);
  const boardId = board?.id;
  const onLeaveFocusFormItem = useCallback(
    (e) => {
      if (boards.length === 0) return;
      if (!board) return;
      if (!boardListRef.current && !boardListRef.current) return;
      if (!boardListRef.current.contains(e.target) && !buttonsRef.current.contains(e.target)) {
        dispatch({ type: TYPES.GET_NO_POST });
        dispatch({ type: TYPES.GET_NO_BOARD });
        setIsSelected(false);
      }
    },
    [boards, board],
  );
  const onAddBoard = useCallback(() => {
    dispatch({
      type: TYPES.ADD_BOARD_REQUEST,
      data: {
        id: Date.now(),
        title: '제목없음',
      },
    });
  }, [boards]);
  const onRemoveBoard = useCallback(() => {
    if (boardId) {
      dispatch({
        type: TYPES.SHOW_MESSAGE_BOX,
        data: { toward: 'BOARD', object: { boardId } },
      });
    }
  }, [boardId]);
  const onChangeBoard = useCallback(
    (item) => () => {
      setIsSelected(true);
      showBoards(false);
      dispatch({ type: TYPES.GET_BOARD_REQUEST, data: item.id });
    },
    [],
  );
  const addBoard = useDebounce(onAddBoard, 100);
  const removeBoard = useDebounce(onRemoveBoard, 100);
  useEffect(() => {
    dispatch({ type: TYPES.GET_ALL_BOARDS_REQUEST });
  }, []);
  useEffect(() => {
    if (removeBoardStatus === STATUS.DONE) {
      setIsSelected(false);
    }
  }, [removeBoardStatus]);
  return (
    <Container onClick={onLeaveFocusFormItem} isShowBoardList={isShowBoardList}>
      <div>
        {getAllBoardsStatus === STATUS.DONE && boards.length !== 0 && (
          <Boards ref={boardListRef}>
            {boards.map((item) => {
              const { id, title } = item;
              return (
                <BoardItem selected={id === boardId} key={id} onClick={onChangeBoard(item)}>
                  <>{id === boardId ? <span>🌟 {title || '제목없음'}</span> : <span>{title || '제목없음'}</span>}</>
                </BoardItem>
              );
            })}
          </Boards>
        )}
      </div>
      {getAllBoardsStatus === STATUS.DONE && (
        <GridBoxWrapper ref={buttonsRef} columns={2}>
          <Button style={{ height: '2rem' }} isFilled onClick={addBoard}>
            <CustomFAIcon icon={faPlus} />
          </Button>
          <Button style={{ height: '2rem' }} disabled={!isSelected} onClick={removeBoard}>
            <CustomFAIcon icon={faMinus} />
          </Button>
        </GridBoxWrapper>
      )}
    </Container>
  );
};

BoardList.propTypes = {
  isShowBoardList: PropTypes.bool.isRequired,
  showBoards: PropTypes.func.isRequired,
};

export default memo(BoardList);
