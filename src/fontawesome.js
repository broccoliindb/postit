import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styled from 'styled-components';

export const addIcon = (icon) => {
  library.add(icon);
};

export const CustomFAIcon = styled(FontAwesomeIcon)`
  opacity: ${(props) => (props.opacity ? props.opacity : '1')};
  color: ${(props) => props.color || 'inherit'};
  width: ${(props) => props.width || '1rem'};
  height: ${(props) => props.height || '1rem'};
`;
