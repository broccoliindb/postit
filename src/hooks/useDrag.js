import { useState, useMemo, useEffect } from 'react';

const useDrag = (startLoc, setIsContentEditMode) => {
  const [dragInfo, setDragInfo] = useState({
    isDragging: false,
    origin: { x: 0, y: 0 },
    translation: startLoc,
    lastTranslation: startLoc,
  });

  const { isDragging } = dragInfo;
  const handleMouseDown = (e) => {
    if (e.button !== 0) return;
    setIsContentEditMode(false);
    e.stopPropagation();
    e.preventDefault();
    setDragInfo({
      ...dragInfo,
      isDragging: true,
      origin: { x: e.clientX, y: e.clientY },
    });
  };

  const handleMouseMove = (e) => {
    e.stopPropagation();
    e.preventDefault();
    const { origin, lastTranslation } = dragInfo;
    if (isDragging) {
      setDragInfo({
        ...dragInfo,
        translation: {
          x: lastTranslation.x + e.clientX - origin.x,
          y: lastTranslation.y + e.clientY - origin.y,
        },
      });
    }
  };

  const handleMouseUp = (e) => {
    e.stopPropagation();
    e.preventDefault();
    setIsContentEditMode(true);
    e.target.focus();
    const { translation } = dragInfo;
    if (isDragging) {
      setDragInfo({
        ...dragInfo,
        isDragging: false,
        lastTranslation: {
          x: translation.x,
          y: translation.y,
        },
      });
    }
  };

  const handleMouseLeave = (e) => {
    e.stopPropagation();
    e.preventDefault();
    const { translation } = dragInfo;
    if (isDragging) {
      setDragInfo({
        ...dragInfo,
        isDragging: false,
        lastTranslation: {
          x: translation.x,
          y: translation.y,
        },
      });
    }
  };
  const location = {
    position: 'absolute',
    left: `${dragInfo.translation.x}px`,
    top: `${dragInfo.translation.y}px`,
  };
  return {
    isDragging,
    location,
    handleMouseDown,
    handleMouseMove,
    handleMouseUp,
    handleMouseLeave,
  };
};

export default useDrag;
