import { useEffect, useState, useCallback } from 'react';

const resultBoxes = {};
const checkWithInputKeys = (compoundInfo, e) => {
  const normalKey = e.key.toLowerCase().length === 1 ? e.key.toLowerCase() : null;
  for (const props in compoundInfo) {
    if (compoundInfo.hasOwnProperty(props)) {
      resultBoxes[props] = false;
      if (props !== 'key' && compoundInfo[props] === e[props]) {
        resultBoxes[props] = true;
      }
      if (props === 'key' && compoundInfo.key === e.key) {
        resultBoxes[props] = true;
      }
    }
  }
  return resultBoxes;
};

const useHotkey = (hotkey, func) => {
  if (typeof useHotkey !== 'string' && typeof func !== 'function') return;

  // 입력핫키
  const regitKeys = hotkey.split('+');

  // 조합키 디폴트설정 여기선 alt,ctrl,일반키 조합만 사용. 추후에 필요시 다른키 추가
  const compoundKeyInfo = {
    altKey: false,
    ctrlKey: false,
    key: null,
  };

  const keydownHandler = (e) => {
    if (typeof e.key !== 'string' || typeof e.code !== 'string') return;

    // 입력된핫키가 조합키인지 여부체크
    const isCompoundCombi = regitKeys.map((key) => {
      const lowerkey = key.toLowerCase();
      switch (lowerkey) {
        case 'ctrl':
          compoundKeyInfo.ctrlKey = true;
          break;
        case 'alt':
        case 'hangulmode':
        case 'meta':
        case 'opt':
        case 'win':
          compoundKeyInfo.altKey = true;
          break;
        default: {
          if (lowerkey.length === 1) {
            compoundKeyInfo.key = lowerkey;
          }
        }
      }
      return checkWithInputKeys.bind(null, compoundKeyInfo);
    });
    if (isCompoundCombi.length > 0) {
      const result = isCompoundCombi[0](e);
      const decisionArray = [];
      for (const info in result) {
        if (result.hasOwnProperty(info)) {
          decisionArray.push(result[info]);
        }
      }
      if (!decisionArray.includes(false)) {
        func(e);
      }
    }
  };
  useEffect(() => {
    window.addEventListener('keydown', keydownHandler);
    return () => {
      window.removeEventListener('keydown', keydownHandler);
    };
  });
};

export default useHotkey;
