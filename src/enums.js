export const LOAD_STATUS = {
  LOADING: 'LOADING',
  DONE: 'DONE',
  ERROR: 'ERROR',
};
