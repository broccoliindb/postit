import React from 'react';
import PostItApp from './components/PostItApp';
import GlobalStyle from './styles/GlobalStyle';

const App: React.FC = () => {
  return (
    <div className="App">
      <PostItApp />
      <GlobalStyle />
    </div>
  );
};

export default App;
