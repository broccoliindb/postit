## 브라우저상에서 동작하는 포스트잇 보드 제작

⭐[배포링크](https://broccoli-postit.netlify.app/)를 통해 방문확인가능합니다.
### 1. 요구사항

- [X] 브라우저 상에서 동작하는 포스트잇 보드를 제작해주세요.
- [X] 레이아웃은 좌측의 보드목록, 우측의 보드로 구성됩니다
- [X] 보드목록 최하단의 [+] 버튼을 누르면 새로운 보드가 생성됩니다.
- [X] 보드의 상단에는 보드명이 나타납니다. 보드명을 클릭하여 수정할 수 있습니다.
- [X] 보드 바닥을 더블클릭하면, 빈 포스트잇이 생성됩니다.
- [X] 포스트잇의 제목과 본문을 클릭하여 수정할 수 있습니다.
- [X] 포스트잇의 타이틀 영역 우측 상단의 [–] 버튼을 클릭하여 본문 영역을 숨겼다 보여줄 수 있습니다.
- [X] 포스트잇의 타이틀 영역 우측 상단의 [X] 버튼을 클릭하여 메모를 삭제할 수 있습니다. 이 때, 제목이나 본문에 내용이 있을 경우에는 “정말 삭제하시겠습니까?” 라는 확인 다이얼로그가 나타납니다.
- [X] `ctrl`(or `cmd`)+ `alt` + `N` 을입력하면, 빈 포스트잇이 생성된 후 제목을 수정할 수 있는 상태가 됩니다.
- [X] 페이지 새로고침을 해도 데이터가 유지됩니다. (local storage, indexed db 등을 이용해주세요. 서버를 추가로 개발하실 필요는 없습니다.)

### 2. 옵션 요구사항
- [X] 포스트잇의 타이틀을 드래그하여 위치를 옮길 수 있습니다.
- [X] 포스트잇의 테두리 부분을 드래그하여, 포스트잇의 크기를 조절할 수 있습니다.
- [X] [이 부분](https://www.notion.so/afc136a0028b4dc098074d2ca76dfe1f) 구현 방식을 redux-saga를 이용해 구현해주세요. 변경된 데이터를 persist storage에 저장하는 방식은 500ms 기준으로 debounce 처리되어야 합니다.

### 3. 사용패키지

- idb: indexedDB를 프로미시하게 사용할 수 있도록 함.
- styled-components, styled-reset
- redux, redux-saga, immer
- prop-types
- fontawesome

### 4. 소스구성
    src
    ├──styles
    │    └──  GlobalStyles
    ├──components   
    │    ├── Board
    │    ├── BoardList
    │    ├── MessageBox
    │    ├── Post
    │    ├── PostItApp
    │    └── Posts
    ├──controls
    ├──hooks
    │    ├── useDebounce
    │    ├── useDrag
    │    └── useHotkeys
    ├──reducers
    │    ├── board
    │    ├── post
    │    └── rootReducer
    ├──sagas
    │    ├── board
    │    ├── post
    │    └── index
    └──store
    │       └── configureStore   
    ├──db 
    ├──enums
    └──fontawesome

### 3. 프로그램 설명

1. 브라우저디비사용: 요구사항에 새로고침시에도 데이터가 유지될 수 있도록 indexedDB 를 사용했습니다. 

> db에는 board, post 두개의 스토어가 있고 
```
board: id, title
post: id, title, content, boardId
```
로 구성되엉 있습니다.

2. 데스크탑과 모바일에 따라 반응형으로 UI를 구현했습니다. 모바일의 경우는 버튼을 통해서 메모를 추가할 수 있습니다.

3. 상태관리를 redux-saga를 통해 구현했고 텍스트가 변경되는 액션의 경우는 500ms의 debounce를 활용했습니다.

4. 드래그의 경우 좀더 넓은 메모(포스트)에 이벤트를 바인딩하였고 메모가 minimal해지는 경우를 위해 메모타이틀에도 같은 이벤트를 바인딩했습니다.

5. 메모의 사이즈 변경은 css resize를 활용하였고, 상하좌우로 확장가능합니다.

6. 핫키의 경우 ctrl+alt+n을 등록했고, 추가로 useHotkey 훅을 활용해서 추가로 
기능등록 가능합니다. 핫키를 이용해 생성시에는 메모타이틀에 포커싱을 주었습니다.

7. 삭제시 메세지를 띄워 확인절차를 거칩니다. 추가로 보드삭제기능이 있습니다. 보드가 삭제될 시에는 보드가 부모로가지는 포스트도 모두 삭제됩니다. 
